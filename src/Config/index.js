/**
 * Configuration 
 * 
 */

 if(process.env.NODE_ENV!=='production'){
     require('dotenv').config();
 }

 module.exports = {
     PORT : process.env.PORT || 5000,

     Stripe:{
         apiKey: process.env.STRIPE_TEST_SECRET_KEY
     }
 }