
 /**
  * MAIN ENTRY POINT
  * 
  */

  const express = require('express');
  const loaderFunc = require('./Loaders/index');

 function startAsyncServer(){

    const PORT = process.env.PORT || 5000;
    // express instance
    const app = express();

    // set-up loaders
    loaderFunc(app);

    app.listen(PORT, ()=>{
        console.log(`SERVER STARTED ${PORT}`);
    })

 }

 // start the server

 startAsyncServer();