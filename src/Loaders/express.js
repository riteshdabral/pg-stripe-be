/**
 *  LOADS EXPRESS
 * 
 */

 const express    = require('express');
 const cors       = require('cors');
 const bodyParser = require('body-parser');

 const PaymentRoute = require('../Api/Routes/Payment');



/**
 * Express Loader
 * Responsible for handling routes, cors, erros
 * 
 * @param {*} app : an express instance passed from upper level
 * 
 */
 const loadExpress = async (app) =>{
    

    app.use(cors());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended:true}));

    app.get('/', async(req,res)=>{
        res.send(`<h1>Pyment Gateway Implementation (Stripe)</h1>`)
    });

    app.use('/pay',PaymentRoute);

    app.use((req,res,next)=>{
        const err = new Error('Not Found');
        err['status'] = 404;
        next(err);
    });

    app.use((err,req,res,next)=>{

        res.status(err['status']||500);

        res.json({
            message:err.message
        })
    })

 }


 /**
  * MODULE EXPORTS
  * 
  * loadExpress : function (param){}
  */
 module.exports = loadExpress;