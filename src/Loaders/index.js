
/**
 * This file consists of all the loaders
 * 
 */

 const loadExpress = require('./express');

 /**
  * Responsible for loading whatsoever is required before starting the server
  * 
  * @param {*} app : an instance of express(), passed from 'app.js' file
  */
 const defaultLoaderFunc = async(app) => {

    // Load express
    loadExpress(app);

 }



 /**
  * Module exports
  * 
  */
 module.exports = defaultLoaderFunc;