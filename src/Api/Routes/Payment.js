/**
 * Payment Routes
 * 
 */

 const Stripe = require('stripe');
 const config = require('../../Config/index');
 const route  = require('express').Router();


 route.post('/', async(req,res,next)=>{
    try {

        let {email,amount} = req.body;

        amount = parseInt(amount)*100;

        if(!email || isNaN(amount) || amount<=0){
            var err = new Error();
            err.message = 'Bad request, check credentials';
            err['status'] = 400;
            err.name = 'Bad Request';

            throw err;
        }
        // creatng stripe object
        const stripe = Stripe(config.Stripe.apiKey,{apiVersion: '2020-08-27'});

        const paymentIntent = await stripe.paymentIntents.create({
            amount: amount,
            currency: 'inr',
            payment_method_types: ['card'],
            receipt_email:email,
            metadata: {integration_check: 'accept_a_payment'}
        });

        res.status(200).json({clientSecret:paymentIntent['client_secret']});

    } catch (e) {
        console.log('src/api/routes/Paymeant/')
        //handling stripe errors
        if(e['type'] && e['type'].includes('Stripe')){
            let err = new Error();
            err.name = e['type'];
            err.message = e['message'];
            err['status'] = e['raw']['statusCode'];
            console.log(err)
            next(err);
        }
        else{
            next(e);
        }        
    }
 });


 /* M O D U L E   E X P O R T S */

 module.exports = route;