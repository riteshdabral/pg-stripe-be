/**
 * Testing routes
 * 
 */

 const request = require('supertest');
 const express = require('express');
 const loaderFunc = require('../src/Loaders/index');
 const app = express();

 describe('TESTING PAYMENT ROUTE', ()=>{

    beforeAll(()=>{
        loaderFunc(app);
    })


    it('POST /pay/ ', (done)=>{
        request(app).post('/pay/')
        .send({"email":"123@xyz.com", "amount":200})
        .expect(200)
        .then(res=>{
            expect(res).not.toBeNull();
            expect(res).not.toBeUndefined();
            done();
        })
    });


    it('Do not POST /pay/ ', (done)=>{
        request(app).post('/pay/')
        .send({"email":"123@xyz.com", "amount":"sds200xx"})
        .expect(400,done)
    });

    
    it('Do not GET /pay/abcd ', (done)=>{
        request(app).get('/pay/abcd')
        .expect(404,done)
    });

 });