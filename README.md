# [***`PG-stripe-BE`***](https://pg-stripe-be.herokuapp.com) 

Payment Gateway application based on NodeJS and Stripe APIs (test mode only) and CI/CD pipeline

## Requirements
  * NodeJS (curr ver - 12)
  * Browser
  * Postman (for testing APIs)

## Scripts
The project consists of well written and easily understandable test cases and descriptions.
#### `npm start`
     - This starts the project in development/production mode
#### `npm test`
     - This tests the functionings

## Important
    - Make sure to add an `.env` file within root directory and add `STRIPE_TEST_SECRET_KEY` as key and `<YOUR STRIPE API KEY>` as value

## Steps
1. run command `git clone https://gitlab.com/riteshdabral/pg-stripe-be.git`
2. Within the root directory, run command `npm install`
3. Run the tests to make sure everything is working fine
4. If all test cases pass, run command `npm start` or `nodemon` from root directory
5. Open your browser or postman application and check the APIs

## Live application
https://pg-stripe-be.herokuapp.com/
